package com.ags.services;

import java.io.InputStream;

public class HelloWorld {
	public float addValue(float value){
		return (value+10);
	}
	
	public float subtractValue(float value){
		return(value-10);
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData){
		String result="CrunchifyRESTService Successfully started";
		return Response.status(200).entity(result).build();
	}
}
